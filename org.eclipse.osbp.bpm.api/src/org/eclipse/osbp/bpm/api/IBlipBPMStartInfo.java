/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm.api;

import java.util.List;

import org.eclipse.osbp.dsl.common.datatypes.IDto;

public interface IBlipBPMStartInfo {
	public String getProcessId();
	public boolean isLogging();
	
	/**
	 * @return the resource name defining the BPM process definition in BPMN2 notation.
	 */
	public String getResourceName();

	/**
	 * @return the full qualified name of the function group
	 */
	public String getFunctionGroupFqn();
	
	/**
	 * @return the full qualified name of the workload dto or null
	 */
	public String getWorkloadDtoFqn();

	/**
	 * @return the process single instance workload dto, which has to put into the bpm process context
	 */
	public IDto createWorkloadDto();

	/**
	 * @param blipId the process unique id inside the blip
	 * @return the BPM human task defined by blipId
	 */
	public IBlipBPMUserTask getBpmHumanTaskForBlipId(String blipId);

	/**
	 * @param bpmId the process unique id inside the BPM definition
	 * @return the BPM human task defined by bpmId
	 */
	public IBlipBPMUserTask getBpmHumanTaskForBpmId(String bpmId);

	/**
	 * @param blipId the process unique id inside the blip
	 * @return the bpm item defined by blipId, which must be able to modify the workload
	 */
	public IBlipBPMWorkloadModifiableItem getWorkloadMofifiableBpmItemForBlipId(String blipId);

	/**
	 * @param bpmId the process unique id inside the BPM definition
	 * @return the bpm item defined by bpmId, which must be able to modify the workload
	 */
	public IBlipBPMWorkloadModifiableItem getWorkloadMofifiableBpmItemForBpmId(String bpmId);
	
	/**
	 * @param blipId the process unique id inside the blip
	 * @return the BPM item defined by blipId
	 */
	public IBlipBPMItem getBpmItemForBlipId(String blipId);
	
	/**
	 * @param bpmId the process unique id inside the BPM definition
	 * @return the BPM item defined by bpmId
	 */
	public IBlipBPMItem getBpmItemForBpmId(String bpmId);
	
	/**
	 * @return the list of all BPM service tasks in this blip
	 */
	public List<IBPMServiceTask> getServiceTasks();
}
