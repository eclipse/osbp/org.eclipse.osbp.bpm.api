/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.bpm.api;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IBPMEngine {
	static final String BPM_HUMANTASK_LOCALE = "en-US";
	
	enum BPMResultSeverity {
		ERROR, WARNING, INFO;
	}
	class BPMError {
		private String resource;
		private BPMResultSeverity severity;
		private String message;
		private int[] lines;
		
		public BPMError(String resource, int [] lines, int severity, String message) {
			this.resource = resource;
			this.lines = lines;
			this.severity = BPMResultSeverity.values()[severity];
			this.message = message;
		}
		
		public BPMResultSeverity getSeverity() {
			return severity;
		}
		
		public String getMessage() {
			return message; 
		}
		
		public int[] getLines() {
			return lines;
		}
		
		public String getResource() {
			return resource;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(this == obj) {
				return true;
			}
			BPMError bpme = (BPMError)obj; 
			if(this.message.equals(bpme.getMessage()) &&
				this.resource.equals(bpme.getResource()) &&
				this.severity.equals(bpme.getSeverity()) &&
				this.lines.length == bpme.getLines().length) {
					return true;
			}
			return false;
		}
		
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(severity.toString());
			sb.append(" in ");
			sb.append(resource);
			sb.append("\n");
			if(lines.length > 0 && (lines.length != 1 || lines[0] != -1)) {
				sb.append("in line ");
				for(int i=0; i<lines.length; i++) {
					sb.append(String.format("%d ", lines[i]));
				}
				sb.append("\n");
			}
			sb.append("\n");
			sb.append(message);
			return sb.toString();
		}
	}
	void registerProcess(String processId);
	boolean processHasErrors();
	List<BPMError> getKnowledgeBuilderErrors();
	void startProcess(String processId); 
	void signalEvent(String type, Object event);
	void signalEvent(String type, Object event, long processInstanceId);
	String getResourceName(String processId);
	void initBPMUsers();
	Set<String> getGroups();
	Set<String> getUsers();
	void addProcessEventListener(Object listener);
	void removeProcessEventListener(Object listener);
	void addTaskEventListener(Object listener);
	void removeTaskEventListener(Object listener);
	Object getProcessInstance(Object taskInformationObject);
	/**
	 * @param   taskInformationObject   object containing information about the task; it can be<ul>
	 *      <li>process instance of type {@link org.drools.runtime.process.ProcessInstance}</li>
	 *      <li>long value containing the process instance id</li>
	 *      <li>int value containing the process instance id</li>
	 *      <li>task summary of type {@link org.jbpm.task.query.TaskSummary}</li>
	 * </ul>
	 * @return  map with String for key and Object for values
	 */
	Map<String,Object> getProcessVariables(Object processInstanceObject);
	/**
	 * @param   taskInformationObject   object containing information about the task; it can be<ul>
	 *      <li>process instance of type {@link org.drools.runtime.process.ProcessInstance}</li>
	 *      <li>long value containing the process instance id</li>
	 *      <li>int value containing the process instance id</li>
	 *      <li>task summary of type {@link org.jbpm.task.query.TaskSummary}</li>
	 * </ul>
	 * @param   variable                name of the requested variable
	 * @return  value of the variable or null
	 */
	Object getProcessVariable(Object processInstanceObject, String variable);
	/**
	 * @param   taskInformationObject   object containing information about the task; it can be<ul>
	 *      <li>process instance of type {@link org.drools.runtime.process.ProcessInstance}</li>
	 *      <li>long value containing the process instance id</li>
	 *      <li>int value containing the process instance id</li>
	 *      <li>task summary of type {@link org.jbpm.task.query.TaskSummary}</li>
	 * </ul>
	 * @param   variable                name of the variable to be set
	 * @param   value                   new values of the variable
	 */
	void setProcessVariable(Object processInstanceObject, String variable, Object value);
	
	void setProcessMetadata(Object processInstanceObject, String variable, Object value);	

	Object getProcessMetadata(Object processInstanceObject, String variable);
	
	Object reCreateKnowledgeSession(Object taskInformationObject);
	void beginTransaction();
	void commitTransaction();
	void rollbackTransaction();
	Object getVariable(Object kcontext, String variable, Object defaultValue);
	void setVariable(Object kcontext, String variable, Object value);
}
