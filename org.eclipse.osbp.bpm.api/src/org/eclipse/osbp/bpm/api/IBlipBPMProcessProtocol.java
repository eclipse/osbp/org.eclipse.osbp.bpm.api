/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm.api;

import java.util.Map;
import java.util.Set;

/**
 *  Interface for a class, which contains single protocol lines for a process instance.
 *  <br>
 *  Only pure <code>String</code> information can be contained.
 *  <br>
 *  The protocol lines should be sorted in chronological order.
 */
public interface IBlipBPMProcessProtocol extends Map<Long, String> {
	/**
	 *  @param  searchProcessId the process id to be searched for
	 *  @return true if the process instance ids contain the given process id.
	 */
	boolean containsProcessId(long searchProcessId);
	/**
	 *  @return the process instance ids for this protocol.
	 */
	Set<Long> getProcessIds();
	/**
	 *  @param  actualProcessId the actual process id
	 *  @param  line            information to be added
	 */
	void addToProtocol(long actualProcessId, String line);
	/**
	 *  @param  actualProcessId the actual process id
	 *  @param  line            information to be added
	 *  @param  toBeMerged      another protocol instance, which has to be merged into this protocol instance
	 */
	void addToProtocol(long actualProcessId, String line, IBlipBPMProcessProtocol toBeMerged);
	/**
	 *  @return <code>String</code> with the complete protocol
	 */
	String toString();
	String processIdsAsString();
	/**
	 *  @param  processId   the process id for this protocol, if it was not set before
	 */
	void updateProcessId(long processId);
}
