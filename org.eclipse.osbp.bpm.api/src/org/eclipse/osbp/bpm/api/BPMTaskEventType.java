/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */

package org.eclipse.osbp.bpm.api;

public enum BPMTaskEventType {
	Create("cr"), Claim("cl"), Stop("st"), Complete("co"), Fail("fa"), Forward("fo"), Release("re"), Skipped(
			"sk"), Started("be"), // begun

	Suspended("ss"), SuspendedUntil("su"), Resume("rs"), Removed("rm"), SetPriority("sp"),

	AddedAttachment("aa"), DeletedAttachment("da"), AddedComment("ac"), UpdatedComment("uc"),

	Delegated("de"), SetOutput("so"), DeleteOutput("do"), SetFault("sf"), DeleteFault("df"), Activate("at"), Nominate(
			"no"), SetGenericHumanRole("sr"), Expire("ex"), Escalated("es"), Cancel("ca"),

	UnknownUserEvent("us");

	private String type;

	private BPMTaskEventType(String t) {
		type = t;
	}

	public String getValue() {
		return type;
	}

	public static BPMTaskEventType getTypeFromValue(String type) {
		int hashCode = type.hashCode();
		switch (hashCode) {
		case 3123:
			return BPMTaskEventType.Activate;
		case 3104:
			return BPMTaskEventType.AddedAttachment;
		case 3106:
			return BPMTaskEventType.AddedComment;
		case 3166:
			return BPMTaskEventType.Cancel;
		case 3177:
			return BPMTaskEventType.Claim;
		case 3180:
			return BPMTaskEventType.Complete;
		case 3183:
			return BPMTaskEventType.Create;
		case 3201:
			return BPMTaskEventType.Delegated;
		case 3202:
			return BPMTaskEventType.DeleteFault;
		case 3211:
			return BPMTaskEventType.DeleteOutput;
		case 3197:
			return BPMTaskEventType.DeletedAttachment;
		case 3246:
			return BPMTaskEventType.Escalated;
		case 3251:
			return BPMTaskEventType.Expire;
		case 3259:
			return BPMTaskEventType.Fail;
		case 3273:
			return BPMTaskEventType.Forward;
		case 3521:
			return BPMTaskEventType.Nominate;
		case 3635:
			return BPMTaskEventType.Release;
		case 3643:
			return BPMTaskEventType.Removed;
		case 3649:
			return BPMTaskEventType.Resume;
		case 3667:
			return BPMTaskEventType.SetFault;
		case 3679:
			return BPMTaskEventType.SetGenericHumanRole;
		case 3676:
			return BPMTaskEventType.SetOutput;
		case 3677:
			return BPMTaskEventType.SetPriority;
		case 3672:
			return BPMTaskEventType.Skipped;
		case 3139:
			return BPMTaskEventType.Started;
		case 3681:
			return BPMTaskEventType.Stop;
		case 3680:
			return BPMTaskEventType.Suspended;
		case 3682:
			return BPMTaskEventType.SuspendedUntil;
		case 3742:
			return BPMTaskEventType.UnknownUserEvent;
		case 3726:
			return BPMTaskEventType.UpdatedComment;
		default:
			throw new IllegalStateException("Unknown type: " + type);
		}
	}
}
