/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm.api;

import java.util.List;

import org.eclipse.osbp.dsl.common.datatypes.IDto;

/**
 * Interface for BPM items, whose workload dto are modifiable.
 * <br>
 * The workload is only modifiable, if the bpm item operates on dtos, which are direct references of the workload dto itself!
 * <br>
 * <b>Adding or removing any operative dtos inside the workload dto does not modify any persisted entities
 * in the product persistence.</b>
 * <br>
 * Modifying dtos in the product persistence should be handled inside an explicit modelled {@link AbstractBPMPersistTask}
 */
public interface IBlipBPMWorkloadModifiableItem extends IBlipBPMItem {

	/**
	 * @param workloadDto the process workload dto
	 * @param operativeDto the operative dto to be added
	 * add an operative dto to the process workload dto
	 */
	public void addOperativeDto(IDto workloadDto, IDto operativeDto);

	/**
	 * @param workloadDto the process workload dto
	 * @param operativeDtos the operative dtos to be added
	 * add operative dtos to the process workload dto
	 */
	public void addOperativeDtos(IDto workloadDto, List<IDto> operativeDtos);

	/**
	 * @param workloadDto the process workload dto
	 * remove all operative dtos from the process workload dto
	 */
	public void removeAllOperativeDtos(IDto workloadDto);

	/**
	 * @param workloadDto the process workload dto
	 * @param operativeDto the operative dto to be removed
	 * remove an operative dto from the process workload dto
	 */
	public void removeOperativeDto(IDto workloadDto, IDto operativeDto);

	/**
	 * @param workloadDto the process workload dto
	 * @param operativeDtos the operative dtos to be removed
	 * remove operative dtos from the process workload dto
	 */
	public void removeOperativeDtos(IDto workloadDto, List<IDto> operativeDtos);
}
